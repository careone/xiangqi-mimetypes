#!/bin/bash
# filename: mk-xiangqi-logo.sh

# ver 1.1, 2020-1-23, by Careone
APPNAME="mk-xiangqi-logo"
APPVER="1.1" #2020-1-23

global_dir="/usr/share/xiangqi/icons"
pic_dir="$HOME/xiangqi/logo"

#back_pic="$HOME/xiangqi/logo/fen_29x29.png"
back_pic="$HOME/xiangqi/logo/back_64x64.png"


PSIZE=64	#Picture size. 64 or 48
SUBDIR="$PSIZE"

BACK_PIC="xiangqi-logo_${PSIZE}x${PSIZE}.png"

back_pic=

declare -a tmp_pic
tmp_pic[0]="_new_logo.png"
tmp_pic[1]="_40x1.png"
tmp_pic[2]="_suffix_pic.png"
#tmp_pic[2]="application-xiangqi-XXX.png"
tmp_pic[3]=

## 图标绘制流程：
# 1. 画一个 40x1 的透明图片 pic1
# 2. 在 pic 下方，以 label 方式，添加扩展名的大写字母，使用背景色 BG1
# 3. 把第2步生成的图片， crop 裁切为 40x?
# 4. 把透明 29x29 的 FEN 十字坐标，宽度调为40；顶部+4,底部+2像素，使用背景色 BG2
# 5. 把第3,4步生成的图片，-append 上下拼接
# 6. 图片扩展为 48x48,透明背景

CNFONT=
declare -a cnfont
## default font and tested: cnfont[0], Alibaba-PuHuiTi-Bold.ttf
cnfont[0]="/usr/share/fonts/truetype/alibaba/Alibaba-PuHuiTi-Bold.ttf"
#
cnfont[1]="/usr/share/fonts/truetype/wqy/wqy-microhei.ttc" 
cnfont[2]="/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc" 

cnfont[3]="/usr/share/fonts/truetype/oppo/OPPOSans-B.ttf"
# 提醒：OPPOSANS 字体 的大写字母 I， 和小写字母 l ，很难识别。

POINTSIZE="16"
ORIG_POINTSIZE=16	#仅用于4个字母的 DPXQ，使用小号字体后，立即恢复原字号，
	# 才不会影响后续其它的扩展名只有3个字母的图标制作。

declare -i ID=1

declare -a suffix
suffix=( FEN XFEN PGN EPD CBL CBR CBF CCM CHE CHN DXQ MXQ PFC UBB XGF XQF XQI )
#suffix=( XGF )

## 各种中国象棋棋谱格式的图标配色方案. 以 64x64为例:
# * 图标特征：竖向长方形，右上角带折角效果。
# 说明：C1 - C4: 代表色区1 到 色区4。FG 代表文字颜色，BG 代表背景色。
# ** 色区1：外框架。包括右上角折角效果
# ** 色区2：上部十字架图案，以及背景。
# ** 色区3：下部棋谱格式扩展名文字，以及背景。
# ** 色区4：四周的空白填充部分。默认为透明背景。
# -------
# * 图标主要配色：
#	1. 浅白色1: #e0e0e0
#	2. 深灰色1: #303030
#	3. 棋盘黄1: #945a25
#	4. 红色:	red
## ===========================	

COLW="#e0e0e0"	# 自定义颜色 White, 浅白色1
COLK="#303030"	# 自定义颜色 blacK，深灰色1
#COLY="#f08040"	# 自定义颜色 Yellow，棋盘黄色1
COLY="#e08000"
COLB="#7070d0"  # 自定义颜色 Blue，天蓝色1

#f08040	#棕色，如 UBB 图标
#
C1OLD="$COLK"	#图标原始模板中，外框默认颜色=深灰1
C1NEW=		#部分图标需要在最后把外框替换成指定颜色，如 CBL 格式，替换成红色。

C2FG=
C2BG=
C3FG=
C3BG=

C4OLD="none"
C4NEW=

### body ###
echo " $APPNAME $APPVER "
echo " draw Xiagnqi logoes (size: ${PSIZE}x${PSIZE})..."

# tag 60: 
# SUFFIX 定位坐标 x1,y1 修正量：以 FEN 为参考：
# 不动项：FEN，UBB，EPD，CHE，CBR,XQF
# 左移项：PGN-1, MXQ-3，CHN-2,CCM-2,DPXQ-6
# 右移项：CBL+1, CBF+1,XQI+1,PFC+1
#
# 青色Cyan、品红色Magenta、黄色Yellow 


# check 1: check base picture
# 先在个人目录下查找，再到系统目录。
# 如果两处都没找到图标底图模板，再报错提示，找不到个人目录下的底图文件。
  if [ -s "$pic_dir/$BACK_PIC" ]; then
    back_pic="$pic_dir/$BACK_PIC"
    echo " Found base picture: $back_pic"    
  elif [ -s "$global_dir/$BACK_PIC" ]; then
    back_pic="$global_dir/$BACK_PIC"
    echo " Found base picture: $back_pic"
  else 
    back_pic="$pic_dir/$BACK_PIC"
    echo " Error: no base picture found: $back_pic ! abort." 1>&2
    exit 1;   
  fi
   
# step 2: check dir    
if [ ! -d "$pic_dir/$SUBDIR" ]; then 
  mkdir -p "$pic_dir/$SUBDIR"
fi

# setp 3：check font
  until [ -n "$CNFONT" ]; do
    for a in "${cnfont[@]}"; do
      if [ -s "$a" ]; then
        CNFONT="$a";   echo "  找到字体: $CNFONT" ;
        break;
      fi
    done
    
    if [ -z "$CNFONT" ]; then
      echo "  强制使用字体: ${cnfont[1]}"
    fi     
  done

# step 4: ready to go

cd "$pic_dir"
#
echo

for a in "${suffix[@]}"; do

  SUFFIX=`echo "$a" | tr 'a-z' 'A-Z'` #force Uppercase

  case "$SUFFIX" in
    FEN) # 下部黑底，白字
	C1NEW="$COLK" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLW" ; C3BG="$COLK"
	C4NEW="none" ;
	x1=5; y1=13; x2=12; y2=45
	;;

    XFEN) # 下部白底，黑字
	C1NEW="$COLK" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLK" ; C3BG="$COLW"
	C4NEW="none" ;
	ORIG_POINTSIZE=16;
	POINTSIZE=14; #四个字母，使用小一点的字体
	x1=2; y1=13; 
	x2=12; y2=45
	;;
#	
    CBL) # 红底，白字; 红框
	C1NEW="red" ;
	C2FG="$COLW" ; C2BG="red"
	C3FG="$COLW" ; C3BG="red"
	C4NEW="none" ;
	x1=6; 
	y1=13; x2=12; y2=45
	;;	
		
    CBF) # f0ff70 浅黄底2,红字
	C1NEW="$COLK" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="red" ; C3BG="#f0ff70"
	C4NEW="none" ;
	x1=6; 
	y1=13; x2=12; y2=45
	;;
		
    CBR) #  红底，白字
	C1NEW="$COLK" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLW" ; C3BG="red" 
	C4NEW="none" ;
	x1=5; 
	y1=13; x2=12; y2=45
	;;
		
    CCM) # #00f0dc cyan 青底，黑字
	C1NEW="$COLY" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLK" ; C3BG="cyan" 
	C4NEW="none" ;
	x1=3; 
	y1=13; x2=12; y2=45
	;;
	
    CHE) # #fbc735 黄底，红字; 
	C1NEW="$COLY" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLK" ; C3BG="#f0c030" 
	C4NEW="none" ;
	x1=5; 
	y1=13; x2=12; y2=45
	;;
	
    CHN) # xqi, 背景土黄色 #945a25
	C1NEW="$COLY" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLW" ; C3BG="#906020" 
	C4NEW="none" ;
	x1=3; 
	y1=13; x2=12; y2=45
	;;

		
    DXQ) #配色同 UBB
	C1NEW="$COLK" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLK" ; C3BG="#f08040"
	C4NEW="none" ;
	x1=4; 
	y1=13; x2=12; y2=45
	;;
	
# DPXQ: disabled and renamed to DPX
    DPXQ) # 棕底 #f37e42，黑字
	C1NEW="$COLK" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLK" ; C3BG="#f08040"
	C4NEW="none" ;
	ORIG_POINTSIZE=16;
	POINTSIZE=14; #四个字母，使用小一点的字体
	x1=1; y1=13; 
	x2=12; y2=45
	;;

    EPD) # 下部白底，蓝字 
	C1NEW="$COLK" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLB" ; C3BG="$COLW"
	C4NEW="none" ;
	x1=6; 
	y1=13; x2=12; y2=45
	;;


    MXQ) # 
	C1NEW="$COLY" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLK" ; C3BG="magenta"
	C4NEW="none" ;
	x1=3; 
	y1=13; x2=12; y2=45
	;;
		
    PFC) # 绿底，白字 
	C1NEW="$COLK" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLW" ; C3BG="green"
	C4NEW="none" ;
	x1=6; 
	y1=13; x2=12; y2=45
	;;	

    PGN) # 上部黄底（#dfbe88，待调），黑字（不变）；下部白字（不变），黑底(待调)；黑框（不变） 
    	# C2BG="#c09040"
	C1NEW="$COLY" ;
	C2FG="$COLK" ; C2BG="#c09040" 
	C3FG="$COLW" ; C3BG="$COLY"
	C4NEW="none" ;
	x1=4; 
	y1=13; x2=12; y2=45
	;;
		
    UBB) # 棕底 #f08040，黑字
	C1NEW="$COLK" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLK" ; C3BG="#f08040"
	C4NEW="none" ;
	x1=5; 
	y1=13; x2=12; y2=45
	;;

# ver 1.1 added
    XGF) # 谢谢象棋棋谱格式（二进制）,蓝底白字；黑框
	# 3060a0 淡蓝；c01010 红；5070b0 白蓝
	C1NEW="$COLY" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLW" ; C3BG="#3060a0" 
	C4NEW="none" ;
	x1=5; y1=13; x2=12; y2=45
	;;
# ==============
				  
    XQF) #
	C1NEW="$COLY" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLK" ; C3BG="$COLB" 
	C4NEW="none" ;
	x1=5; y1=13; x2=12; y2=45
	;;

    XQI) # xqi, 背景土黄色 #945a25
	C1NEW="$COLY" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLW" ; C3BG="#906020" 
	C4NEW="none" ;
	x1=6; 
	y1=13; x2=12; y2=45
	;;

    *) # 同 PGN 
	C1NEW="$COLK" ;
	C2FG="$COLW" ; C2BG="$COLK"
	C3FG="$COLW" ; C3BG="$COLK"
	C4NEW="none" ;
	x1=5; y1=13; x2=12; y2=45
	;;	
  esac
	
LOWER_SUFFIX=`echo "$SUFFIX" | tr 'A-Z' 'a-z'`
tmp_pic[3]="application-xiangqi-${LOWER_SUFFIX}.png"

echo -n " * logo $ID: ($SUFFIX)... "

### --------
# step 1: 复制并调整底图

## 调整 PGN，CBL 等几种常见棋谱的图标颜色，有更快、更明显的视觉查找效果
#
# * 48 把 test.png 中的所有白色都改为透明。包括2%的色差范围
# convert test.png -fuzz 2% -transparent white alpha.png
#
# * 49 把 test.png 的透明背景改为白色
# convert test.png -background white -flatten white-back.png
# 或者直接 convert test.png -flatten white-back.png
#
#   ***  3、替换相同颜色的区域（指定颜色）
# 指定颜色差异程度（10%），替换颜色（黑色），被替换颜色（白色）。将整张图片中的指定颜色全部替换。
# convert 2.jpg -alpha set -channel RGBA -fuzz 10% -fill "rgb(0,0,0)" -opaque "rgb(255,255,255)" new.png 
# opaque = 不透明的
#
#    4、替换不同颜色的区域（指定颜色）
# 指定颜色差异程度（50%），替换颜色（白色），指定颜色（蓝色）。将整张图片中除指定颜色外的颜色全部替换。
# convert 2.jpg -alpha set -channel RGBA -fuzz 50% -fill "rgb(255,255,255)" +opaque "rgb(0,0,255)" new.png 

  case "$SUFFIX" in
  	CBL | CCM | CHE | CHN | XQI | MXQ | XQF | PGN)
  	# CBL 的外框（即色区1）改成红色 red
  	# CCM | CHE | CHN | XQI | MXQ | XQF, 外框（即色区1）都改成棋盘黄色, 标示着能被 
  	#   gmchess 程序打开
	# PGN 的外框改为蓝色
	
  	convert -depth 8 "$back_pic" -fill "$C1NEW" -opaque "$C1OLD" "${tmp_pic[0]}"
  	;;
  	  	
  	*) cp -f "$back_pic" "${tmp_pic[0]}"
  	;;
  esac

# step 2: 创建一个 41x16 的小图片，用来在下方添加扩展名文字
    convert -size 41x16 xc:"$C3BG" "${tmp_pic[1]}" 

  
# step 3:
# * 39 在图片上加文字
#convert -fill green -pointsize 40 -draw 'text 10,50 "xxx"' foo.png bar.png
#  上面的命令在距离图片的左上角10x50的位置，用绿色的字写下 xxx
#
convert "${tmp_pic[1]}" -fill "$C3FG" -background "$C3BG" -font "$CNFONT" -pointsize "$POINTSIZE" -draw "text $x1,$y1 '$SUFFIX'" "${tmp_pic[2]}" 

  POINTSIZE="$ORIG_POINTSIZE"

#convert -depth 8 "$back_pic" "${tmp_pic[1]}" -geometry +$x2+$y2 -composite "$SUBDIR/${tmp_pic[2]}" 


# step 4:  	
 # tag 370:
 convert -depth 8 "${tmp_pic[0]}" "${tmp_pic[2]}" -geometry +$x2+$y2 -composite "$SUBDIR/${tmp_pic[3]}"
  	
  #file   "$output"
  file "$SUBDIR/${tmp_pic[3]}"
# 
CLEAN_TMP_PIC=0
  if [ "$CLEAN_TMP_PIC" = 1 ]; then
    rm -f "${tmp_pic[0]}" "${tmp_pic[1]}" "${tmp_pic[2]}"
  fi

  sleep 1
  let "ID += 1"
  done
 
 DEBUG=1
  if [ "$DEBUG" = 1 ]; then  
	eog "$SUBDIR/${tmp_pic[3]}" &
  fi
  
exit 0;
